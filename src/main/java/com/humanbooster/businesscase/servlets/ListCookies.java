/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.humanbooster.businesscase.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author aureliendelorme
 */
public class ListCookies extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        if(request.getSession().getAttribute("username") == null){
           request.getRequestDispatcher("user").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListCookies</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Vos cookies </h1>"
                    + "<ul>");
           if(request.getCookies() != null){
              Cookie[] cookies = request.getCookies();
            
            for (Cookie cookie : cookies) {
                out.println("<li>" + cookie.getName() + " - "+cookie.getValue()+"</li>");
            }
           } 
            out.println("</ul>"
                    + "<h2>Ajouter un cookie</h2>");
             out.println("<form method=post>"
                        + "<label>Clé :</label><input type='text' name='key'>"
                        + "<label>Valeur</label><input type=text' name='value'>"
                        + "<input type='submit' value='Envoyer !'>"
                            + "</form>");
             
            out.println("</body>");
            out.println("</html>");
        }
    }
    

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
       if(request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("value")){
        Cookie newCookie = new Cookie(request.getParameter("key"), request.getParameter("value"));
        response.addCookie(newCookie);
        
        response.sendRedirect(request.getContextPath() + "/list-cookies");
       } else {
           this.doGet(request, response);
       }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
