<%-- 
    Document   : question-list
    Created on : 19 oct. 2020, 11:18:28
    Author     : aureliendelorme
--%>

<%@page import="java.util.List" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html xmlns="http://www.w3c.org/1999/xhtml"
        xmlns:h="http://java.sun.com/jsf/html"
        xmlns:p="http://primefaces.org/ui"
        xmlns:pt="http://xmlns.jcp.org/jsf/passthrough">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Les questions</title>
</head>
<body>
    <h1>Liste des questions !</h1>
    <%@include file="parts/menu.jsp" %>
    
    <c:set var="role" value="admin" scope="page" />
    <c:set var="role" value="admin" scope="request" />
    <c:set var="role" value="admin" scope="session" />
    <c:set var="role" value="admin" scope="application" />
    
   
    <c:if test = "${role == 'admin'}">
        <a>Ajouter une question</a>
    </c:if>
    <br>
    <span>${questions.size()} questions :</span>
    <ol>
    <c:forEach items="${ questions }" var="question">
        <li>- <c:out value="${ question }" /></li>
    </c:forEach>
        
    <c:if test = "${questions.size() == 0}">
        <p>Aucune question n'a encore été ajoutée</p>
    </c:if>

</ol>
</body>
</html>
