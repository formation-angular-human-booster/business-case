<%-- 
    Document   : my-account
    Created on : 19 oct. 2020, 10:28:16
    Author     : aureliendelorme
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%! 
            Date dNow = new Date( );
            SimpleDateFormat ft = 
            new SimpleDateFormat ("dd/MM/yyy");
            String dateDuJour = ft.format(dNow);
    %>
 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mon compte</title>
    </head>
    <body>
        <h1>Mon compte !</h1>
        <%@include file="parts/menu.jsp" %>
        <h2>Bonjour <%= request.getSession().getAttribute("username") %>, nous sommes le  <%= dateDuJour %>
    </body>
</html>
