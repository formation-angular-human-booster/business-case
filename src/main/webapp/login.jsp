<%@page import="java.util.List" %>
<%@page import="java.util.Date" %>
<%@ page import="javax.servlet.*,java.text.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Veuillez vous connecter !</title>
</head>
<body>
<h1>Merci de vous connecter !</h1>
<h2>Nous sommes le
        <%
            Date dNow = new Date( );
            SimpleDateFormat ft = 
            new SimpleDateFormat ("dd/MM/yyy");
            out.print(ft.format(dNow));
        %>
    <form method="post" action="user">
        <label>Login</label><input type="text" name="login">
        <label>Password</label><input type="password" name="password">
        <input type="submit" value="Envoyer !">
    </form>
    <ul>
        <%
            if(request.getMethod().equals("POST")){
                List<String> errors = (List<String>) request.getAttribute("errors");
                for(String str : errors){
                    out.println("<li> "+ str +"</li></br>");
                }
            }
        %>
    </ul>
</body>
</html>
